#include <iostream>
#include <iterator>
#include <fstream>
#include <iomanip>
#include <functional>
#include <ctime>
#include <chrono>
#include <cmath>
#include <vector>
#include <numeric>
#include <map>
#include <intrin.h>     //rdtsc()
#include <windows.h>    //QueryPerformanceFrequency()
                        //QueryPerformanceCounter()
#include "statistics.h"


//осмысленное название векторов замеров времени
using TimeVector = std::vector<double>;

//осмысленное название функутора c типом void
using VoidFunc = std::function<void()>;

/*
 *Блок с некоторыми глобальными константами
 *
 */

//частота CPU, заблочил в BIOS
const int64_t freq = 3'500'000'000;

//Размер массива для тестовой функции myFunc
const unsigned long long arraySize = 1'000'000'000;

//Тестовый массив
double* arr = new double[arraySize];



//Функция "пустышка"
int dummy() { return 0; }

//Тестовая функция
void myFunc(double* parr, int len) {
    for(std::size_t i = 0; i < len; ++i) {
        parr[i] = std::sinh(i);
    }
}

/*
 * Все таймеры объявлены шаблонами для того,чтобы
 * принимать на вход все callable-объекты - обычные функции,
 * указатели на них, лямды, связывания. Это удобно, быстро
 * (потери около 5% и то из-за отключенной оптимизации) и просто красиво.
 */

/*
 * таймер, основанный на функции std::clock()
 * функция std::clock() возвращает приблизительное время процессора,
 * используемое процессом с начала выполнения программы в переменную
 * типа std::clock_t
 * Таким образом, замеряем время до старта тестируемой
 * функции, затем после, вычитаем эти 2 значения и делим на
 * количество в секунду. При желании выводить в миллисекундах
 * домножаем результат на 1000
 */
template<typename Func>
double timerClock(const Func& testFuncArg) {
    std::clock_t c_start = std::clock();
    testFuncArg();
    return 1000 * (std::clock() - c_start) / CLOCKS_PER_SEC;
}

/*
 * таймер, основанный на функции __rdtsc()
 * функция __rdtsc() возвращает количество тактов процессора с
 * момента сброса данного значения. Сброс нужен, потому что такты
 * накапливаются очень быстро и не одна переменная не вместит их
 * в себя.
 * Чтобы измерить время с помощью данной функции нужно замерить
 * количество тактов до, после, отнять и поделить на частоту процессора
 * Вашего компьютера, которая и есть количество тактов в секунду.
 * При желании выводить в миллисекундах домножаем результат на 1000.
 */
template<typename Func>
double timerRDTSC(const Func& testFuncArg) {
    __int64 c_start = __rdtsc();
    testFuncArg();
    return 1000 * static_cast<double>((__rdtsc() - c_start)) / freq;
}

/*
 *таймер, основанный на фукнциях QueryPerformanceFrequency()
 *                               QueryPerformanceCounter()
 *
 * функция QueryPerformanceCounter() возвращает количество тиков
 * высокоточного счетчика, имеющегося в процессоре
 * QueryPerformanceFrequency() - частоту, с которой данный счетчик делает
 * тики
 * Если прочитали про предыдущие, то сами догадаетесь, как посчитать время
 */
template<typename Func>
double timerQPC(const Func& testFuncArg) {
    LARGE_INTEGER freq,
                  c_start,
                  c_end;
    QueryPerformanceFrequency(&freq);
    double PFfreq = static_cast<double>(freq.QuadPart) / 1000;
    QueryPerformanceCounter(&c_start);
    testFuncArg();
    QueryPerformanceCounter(&c_end);
    return static_cast<double>(c_end.QuadPart - c_start.QuadPart) / PFfreq;
}

//Лямбда для сокращения кода. Создает адаптер функции myFunc под разный размер length
auto adapter = [](unsigned length) { return std::bind(myFunc, arr, length); };

//Функция нахождения разрешающей способности таймеров
//Принимает на вход этот самый таймер
template<class Func>
double findResolution(const Func& timer, unsigned startSize, unsigned step) {
    unsigned count = 0;
    double result = 0;
    while(count < 10) {
        result = timer(adapter(startSize));
        if (result) {
            count++;
        } else {
            startSize += step;
            count = 0;
        }
    }
    return result;
}

/* Формирует распределение времени величиной в count элементов(по умолчанию 100),
 * полученное от таймера при тестировании функции MyFunc() с длинной массива
 * вовлеченного в обработку arrayLength
 */
template<class Func>
TimeVector createDistribution(const Func& timer, unsigned arrayLength, unsigned count = 100) {
    TimeVector res(count);
    std::fill(res.begin(), res.end(), timer(adapter(arrayLength)));
    return res;
}


/* Формирует линейно возрастающий массив значений времени в count элементов
 * полученное от таймера при тестировании функции MyFunc() с начальной длинной массива
 * startSize и шагом увеличения длины step
 * вовлеченного в обработку arrayLength
 */
template<class Func>
TimeVector createLinear(const Func& timer, unsigned count, unsigned startSize, unsigned step) {
    TimeVector res(count);
    for(auto it = res.begin(); it != res.end(); ++it) {
        *it = timer(adapter(startSize));
        startSize += step;
    }
    return res;
}
/* вывод вектора величин v в выходной поток ostream, изспользуя
 * в качестве разделителя мужду элементами строку delimiter
 */
void outTimeVector(const TimeVector& v, std::ostream& ostream, char const* delimiter) {
    std::copy(v.begin(), v.end(), std::ostream_iterator<double>(ostream, delimiter));
    ostream << std::endl;
}

/* Получение статистических параметров распределения
 */
analysis::Parameters getParameters(const TimeVector& distribution) {
    return analysis::Parameters(analysis::expectation(distribution),
                                analysis::dispersion(distribution),
                                analysis::deviation(distribution));
}

int main(void) {
    //создание ассциативного массива имен таймеров и ссылок на самих
    std::map<std::string, std::function<double(const VoidFunc)>> namedTimers {
        { "timerClock", timerClock<VoidFunc> },
        { "timerRDTSC", timerRDTSC<VoidFunc> },
        { "timerQPC", timerQPC<VoidFunc> }
    };

    //создание ассциативного массива имен таймеров и оптимальных размеров
    //массива для получения распределения
    std::map<std::string, std::pair<unsigned, unsigned>> optimalSize {
        { "timerClock", { 7'000'000, 10'000 } },
        { "timerRDTSC", { 1, 1 } },
        { "timerQPC", { 38'000, 1000 } }
    };

    //создание выходного файлового потока
    std::ofstream fout("D://QT//projects//VPV1_on_CPP//fout.txt");
    if(fout.bad()) {    //проверка для галочки
        std::cerr << "Can't open output file" << std::endl;
        return fout.rdstate();
    }

    std::cout << "Resolution of every timer" << std::endl
              << "timerClock " <<  findResolution(timerClock<VoidFunc>, 10000, 100)<< std::endl
              << "timerRDTSC " <<  findResolution(timerRDTSC<VoidFunc>, 1, 1) << std::endl
              << "timerQPC " <<  findResolution(timerQPC<VoidFunc>, 1, 1) << std::endl << std::endl;

    std::vector<double> x(100);
    std::iota(x.begin(), x.end(), 1);
    for(const auto& timer : namedTimers) {
        std::cout << std::setw(66) << std::cout.fill('-') << std::endl
                  << timer.first << " analysis" << std::endl;
//        std::cout << "Empty function execution time : "
//                  << timer.first << " " << timer.second(dummy) << " ms"
//                  << std::endl;
        std::cout << "Distribution parameters: " << std::endl;
        TimeVector t = createLinear(timer.second, 100, optimalSize[timer.first].first, optimalSize[timer.first].second);
        std::pair factors = analysis::linearRegresion(x, t);
        std::cout << factors.first << " * x + " << factors.second << std::endl;
        std::cout << getParameters(analysis::findRemains(t, factors)) << std::endl;
    }




    fout.close();
    return std::cout.rdstate() && fout.rdstate();
}

