#ifndef STATISTICS_H
#define STATISTICS_H
#include <iostream>
#include <vector>
#include <cmath>
#include <numeric>
#include <algorithm>
#endif // STATISTICS_H



namespace analysis {
    struct Parameters {
        double expectation,
               dispersion,
               deviation;
        Parameters(double m, double d, double s) : expectation(m),
                                                   dispersion(d),
                                                   deviation(s) {}
        friend std::ostream& operator<< (std::ostream &out, const Parameters& Parameters);
    };

    double squareSum(const std::vector<double>&);

    double sum(const std::vector<double>&);

    double xySum(const std::vector<double>&, const std::vector<double>&);

    std::pair<double, double> linearRegresion(const std::vector<double>&,
                                              const std::vector<double>&);

    std::vector<double> findRemains(const std::vector<double>&,
                                    const std::pair<double, double>&);
    //Функция нахождения мат ожидания.
    //v - константная ссылка на вектор значений
    double expectation(const std::vector<double>&);

    //Функция нахождения дисперсии.
    //v - константная ссылка на вектор значений
    double dispersion(const std::vector<double>&);

    //Функция нахождения среднеквадратичного отклонения.
    //v - константная ссылка на вектор значений
    double deviation(const std::vector<double>& v);

}
