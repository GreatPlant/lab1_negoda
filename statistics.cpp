#include "statistics.h"

namespace analysis {

    //Перегруженный оператор вывода для структуры Parameters
    std::ostream& operator<< (std::ostream &out, const Parameters& Parameters) {
        out << "expectation: " << Parameters.expectation << ", "
            << "dispersion: " << Parameters.dispersion << ", "
            << "deviation: " << Parameters.deviation;
        return out;
    }


    double squareSum(const std::vector<double>& v) {
        return std::accumulate(v.begin(), v.end(), 0.0, [&](double sum, double n) {
            return sum += n * n;});
    }

    double sum(const std::vector<double>& v) {
        return std::accumulate(v.begin(), v.end(), 0.0);
    }

    double xySum(const std::vector<double>& x, const std::vector<double>& y) {
        return std::inner_product(x.begin(), x.end(), y.begin(), 0.0);
    }

    std::pair<double, double> linearRegresion(const std::vector<double>& x,
                                              const std::vector<double>& y)
    {
        unsigned n = x.size();
        double xFactor = ( n * xySum(x, y) - sum(x) * sum(y) ) /
                         ( n * squareSum(x) - sum(x) * sum(x) );
        double yFactor = ( sum(y) - xFactor * sum(x) ) / n;
        return std::make_pair(xFactor, yFactor);
    }
    std::vector<double> findRemains(const std::vector<double>& y,
                                    const std::pair<double, double>& factors) {
        std::vector<double> res(y.size());
        size_t x = 1;
        for(auto yi : y) {
            res[x - 1] = yi - (factors.first * x + factors.second);
            x++;
        }
        return res;
    }
    //Функция нахождения мат ожидания.
    //v - константная ссылка на вектор значений
    double expectation(const std::vector<double>& v) {
        return static_cast<double>(std::accumulate(v.begin(), v.end(), 0.0)) / (v.size());
    }

    //Функция нахождения дисперсии.
    //v - константная ссылка на вектор значений
    double dispersion(const std::vector<double>& v) {
        double d = 0;
        double expect = expectation(v);
        for(auto i : v) {
            d += std::pow((i - expect), 2);
        }
        return d / (v.size() - 1);
    }

    //Функция нахождения среднеквадратичного отклонения.
    //v - константная ссылка на вектор значений
    double deviation(const std::vector<double>& v) {
        return std::sqrt(dispersion(v));
    }

}
